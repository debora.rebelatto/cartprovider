import 'package:flutter/foundation.dart';
import 'package:series_app/Models/Carrinho.dart';
import 'package:series_app/Templates/CarrinhoData.dart';

class Counter with ChangeNotifier, DiagnosticableTreeMixin {
  int _counter = 0;
  List<Carrinho>? listCarrinho = CarrinhoData.listCarrinho;

  int get count => _counter;
  List<Carrinho>? get getListCarrinho => listCarrinho;


  void remove(index) {
    listCarrinho!.removeAt(index);
    notifyListeners();
  }

  void decrement(index, quantidade) {
    listCarrinho![index].quantidade = quantidade;
    notifyListeners();
  }

  void add(index, quantidade) {
    listCarrinho![index].quantidade = quantidade;

    notifyListeners();
  }
}
