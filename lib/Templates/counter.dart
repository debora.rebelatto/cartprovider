import 'package:flutter/material.dart';
import 'package:series_app/Provider/CarrinhoProvider.dart';

import 'package:provider/provider.dart';

class CounterQuanti extends StatefulWidget {
  final index;
  CounterQuanti(this.index, { Key? key }) : super(key: key);

  @override
  _CounterQuantiState createState() => _CounterQuantiState();
}

class _CounterQuantiState extends State<CounterQuanti> {
  var counter = TextEditingController(text: '1');

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.remove),
          onPressed: () {
            counter.text = (int.parse(counter.text) - 1).toString();
            context.read<Counter>().decrement(widget.index, int.parse(counter.text));
          },
        ),
        Text('${counter.text}'),
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () {
            counter.text = (int.parse(counter.text) + 1).toString();
            context.read<Counter>().add(widget.index, int.parse(counter.text));
          },
        )
      ],
    );
  }
}