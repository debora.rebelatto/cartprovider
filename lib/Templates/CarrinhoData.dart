import 'package:series_app/Models/Carrinho.dart';

class CarrinhoData {

  static List<Carrinho> listCarrinho = [
    Carrinho(
      id: 1,
      nome: "Celular",
      preco: 1000.00,
      quantidade: 1,
    ),
    Carrinho(
      id: 2,
      nome: "Tablet",
      preco: 2000.00,
      quantidade: 1,
    ),
    Carrinho(
      id: 3,
      nome: "Notebook",
      preco: 3000.00,
      quantidade: 1,
    ),
    Carrinho(
      id: 4,
      nome: "Smartphone",
      preco: 4000.00,
      quantidade: 1,
    ),
    Carrinho(
      id: 5,
      nome: "Smartwatch",
      preco: 5000.00,
      quantidade: 1,
    ),
    Carrinho(
      id: 6,
      nome: "Smartband",
      preco: 6000.00,
      quantidade: 1,
    ),
    Carrinho(
      id: 7,
      nome: "Smartphone",
      preco: 7000.00,
      quantidade: 1,
    ),
    Carrinho(
      id: 8,
      nome: "Smartwatch",
      preco: 8000.00,
      quantidade: 1,
    ),
    Carrinho(
      id: 9,
      nome: "Smartband",
      preco: 9000.00,
      quantidade: 1,
    ),
    Carrinho(
      id: 10,
      nome: "Smartphone",
      preco: 10000.00,
      quantidade: 1,
    ),
  ];
}