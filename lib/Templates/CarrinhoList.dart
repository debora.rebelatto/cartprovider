import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:series_app/Provider/CarrinhoProvider.dart';
import 'package:series_app/Templates/counter.dart';

class MyHomePage extends StatefulWidget {

  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: const Text('Example') ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ListView.builder(
                shrinkWrap: true,
                itemCount: Provider.of<Counter>(context).listCarrinho!.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: IconButton(
                      icon: Icon(Icons.remove),
                      onPressed: () {
                        context.read<Counter>().remove(index);
                      },
                    ),
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${context.watch<Counter>().listCarrinho![index].nome}',
                          // key: const Key('counterState'),
                        ),
                        Text(
                          '${context.watch<Counter>().listCarrinho![index].quantidade}',
                          // key: const Key('counterState'),
                        ),
                      ],
                    ),
                    subtitle: CounterQuanti(index)
                  );
                }
              ),
            ],
          ),
        ),
      ),
    );
  }
}