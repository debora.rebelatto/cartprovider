// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Carrinho.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Carrinho _$CarrinhoFromJson(Map<String, dynamic> json) {
  return Carrinho(
    id: json['id'] as int,
    nome: json['nome'] as String,
    preco: (json['preco'] as num).toDouble(),
    quantidade: json['quantidade'] as int,
  );
}

Map<String, dynamic> _$CarrinhoToJson(Carrinho instance) => <String, dynamic>{
      'id': instance.id,
      'nome': instance.nome,
      'preco': instance.preco,
      'quantidade': instance.quantidade,
    };
