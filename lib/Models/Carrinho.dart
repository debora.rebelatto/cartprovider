import 'package:json_annotation/json_annotation.dart';

part 'Carrinho.g.dart';

@JsonSerializable()

class Carrinho {
  int id;
  String nome;
  double preco;
  int quantidade;

  Carrinho({
    required this.id,
    required this.nome,
    required this.preco,
    required this.quantidade
  });

  factory Carrinho.fromJson(Map<String, dynamic> json) => _$CarrinhoFromJson(json);
  Map<String, dynamic> toJson() => _$CarrinhoToJson(this);
}
